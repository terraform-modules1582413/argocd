variable "tolerations_mapping" {
  default = {
    "NO_SCHEDULE"        = "NoSchedule",
    "PREFER_NO_SCHEDULE" = "PreferNoSchedule",
    "NO_EXECUTE"         = "NoExecute"
  }
}

locals {
  ingress_domain = "${module.name.pretty}.${var.cf_base_domain}"
  tolerations = [
    for taint in var.argocd_taints : {
      key      = taint.key
      value    = taint.value
      effect   = lookup(var.tolerations_mapping, taint.effect)
      operator = "Equal"
    }
  ]
}

resource "helm_release" "argocd_crt" {
  chart            = "${path.module}/certificate"
  name             = "argocd-crt"
  create_namespace = var.argocd_create_namespace_enabled
  namespace        = var.argocd_namespace

  set {
    name  = "key"
    value = var.crt_tls_key
  }

  set {
    name  = "crt"
    value = var.crt_tls_crt
  }

  set {
    name  = "name"
    value = var.crt_name
  }
}

resource "google_service_account" "argocd" {
  project      = var.gcp_project_id
  account_id   = module.name.short
  display_name = "Service account for argocd"
}

resource "google_project_iam_member" "argocd_components_roles" {
  for_each = toset(["argocd-application-controller", "argocd-server"])

  role    = "roles/iam.workloadIdentityUser"
  member  = "serviceAccount:${var.gcp_project_id}.svc.id.goog[${var.argocd_namespace}/${each.value}]"
  project = var.gcp_project_id
}

resource "helm_release" "argocd" {
  name             = "argocd"
  chart            = "argo-cd"
  repository       = "https://argoproj.github.io/argo-helm"
  create_namespace = var.argocd_create_namespace_enabled
  namespace        = var.argocd_namespace
  version          = "5.27.5"

  values = [
    templatefile("${path.module}/values.yaml", {
      ingress_class  = var.ingress_class
      ingress_domain = local.ingress_domain
      crt_name       = var.crt_name
      tolerations    = local.tolerations
      node_selectors = var.argocd_node_selectors
      sa_email       = google_service_account.argocd.email
    })
  ]

  depends_on = [helm_release.argocd_crt]
}

resource "time_sleep" "wait_60_seconds" {
  depends_on = [helm_release.argocd]

  destroy_duration = "60s"
}

resource "cloudflare_record" "argocd" {
  zone_id = data.cloudflare_zone.main.zone_id
  name    = module.name.pretty
  value   = var.ingress_public_ip
  type    = "A"
  ttl     = "300"
  proxied = false
}
