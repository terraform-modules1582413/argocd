variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}

variable "gcp_project_id" {
  type = string
}

variable "cf_base_domain" {
  type = string
}

variable "ingress_class" {
  type = string
}
variable "ingress_public_ip" {
  type = string
}

variable "argocd_create_namespace_enabled" {
  type    = bool
  default = true
}
variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_node_selectors" {
  type    = map(any)
  default = {}
}
variable "argocd_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
  default = []
}

variable "crt_name" {
  type    = string
  default = "argocd-server-tls"
}
variable "crt_tls_key" {
  type      = string
  sensitive = true
}
variable "crt_tls_crt" {
  type      = string
  sensitive = true
}
