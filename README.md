<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.0.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.9.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | ~> 4.0.0 |
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.9.0 |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.argocd](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [google_project_iam_member.argocd_components_roles](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.argocd](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [helm_release.argocd](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.argocd_crt](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [time_sleep.wait_60_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [cloudflare_zone.main](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_argocd_create_namespace_enabled"></a> [argocd\_create\_namespace\_enabled](#input\_argocd\_create\_namespace\_enabled) | n/a | `bool` | `true` | no |
| <a name="input_argocd_namespace"></a> [argocd\_namespace](#input\_argocd\_namespace) | n/a | `string` | `"argocd"` | no |
| <a name="input_argocd_node_selectors"></a> [argocd\_node\_selectors](#input\_argocd\_node\_selectors) | n/a | `map(any)` | `{}` | no |
| <a name="input_argocd_taints"></a> [argocd\_taints](#input\_argocd\_taints) | n/a | <pre>list(object({<br>    key    = string<br>    value  = string<br>    effect = string<br>  }))</pre> | `[]` | no |
| <a name="input_cf_base_domain"></a> [cf\_base\_domain](#input\_cf\_base\_domain) | n/a | `string` | n/a | yes |
| <a name="input_crt_name"></a> [crt\_name](#input\_crt\_name) | n/a | `string` | `"argocd-server-tls"` | no |
| <a name="input_crt_tls_crt"></a> [crt\_tls\_crt](#input\_crt\_tls\_crt) | n/a | `string` | n/a | yes |
| <a name="input_crt_tls_key"></a> [crt\_tls\_key](#input\_crt\_tls\_key) | n/a | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_ingress_class"></a> [ingress\_class](#input\_ingress\_class) | n/a | `string` | n/a | yes |
| <a name="input_ingress_public_ip"></a> [ingress\_public\_ip](#input\_ingress\_public\_ip) | n/a | `string` | n/a | yes |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |
| <a name="input_tolerations_mapping"></a> [tolerations\_mapping](#input\_tolerations\_mapping) | n/a | `map` | <pre>{<br>  "NO_EXECUTE": "NoExecute",<br>  "NO_SCHEDULE": "NoSchedule",<br>  "PREFER_NO_SCHEDULE": "PreferNoSchedule"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_argocd_basic_info"></a> [argocd\_basic\_info](#output\_argocd\_basic\_info) | n/a |
| <a name="output_namespace"></a> [namespace](#output\_namespace) | n/a |
| <a name="output_sa"></a> [sa](#output\_sa) | n/a |
<!-- END_TF_DOCS -->