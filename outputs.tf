output "argocd_basic_info" {
  value = "ArgoCD ${module.name.full} FDQN is ${cloudflare_record.argocd.name}.${var.cf_base_domain} [ ${var.ingress_public_ip} ]"
}

output "namespace" {
  value = var.argocd_namespace
}

output "sa" {
  value = google_service_account.argocd.email
}